import React from 'react';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import { green, red, yellow, blue, purple } from '@material-ui/core/colors';


import { makeStyles } from '@material-ui/core/styles';




import './JobDetail.css';

const styles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      maxWidth: 752,
    },
    demo: {
      backgroundColor: theme.palette.background.paper,
    },
    title: {
      margin: theme.spacing(4, 0, 2),
    },
  }));

const  JobDetail  = ({ selected, targetJob }) => {
    const classes = styles();
    const type = { fullMatch: 1, toLearn: 2, notMatch: 3, plus: 4 };

    const diff = () => {
        var selectedJobMap = {};
        var targetJobMap = {};
        if(selected) {
            for(var i = 0; i < selected.levels.length; i++) {
                var current = selected.levels[i];
                selectedJobMap[current.id_competence] = current;
            }
        }
        if(targetJob) {
            for(var i = 0; i < targetJob.levels.length; i++) {
                var current = targetJob.levels[i];
                targetJobMap[current.id_competence] = current;
            }
        }
        
        var result = {start: selectedJobMap, target: targetJobMap};
        return result;
    }
    const compareSkills = () => {
        var allJobs = [];
        var map = diff();
        var start = map.start;
        var target = map.target;
        var additionSkills = [];

        for(var item in start) {
            var additionSkill = target[item];
            if(!additionSkill) {
                var addition = start[item];
                addition.type = type.plus;
                additionSkills.push(addition);
            }
        }


        for(var skillId in target) {
            var startSkill = start[skillId];
            var targetSkill = target[skillId];

            if(startSkill) {
               if(startSkill.id_competence == targetSkill.id_competence) {
                    if(startSkill.numero_niveau == targetSkill.numero_niveau) {
                        targetSkill.type = type.fullMatch;
                        allJobs.push(targetSkill);
                    }else if(startSkill.numero_niveau < targetSkill.numero_niveau) {
                        targetSkill.type = type.toLearn;
                        allJobs.push(targetSkill);
                    }else if(startSkill.numero_niveau > targetSkill.numero_niveau) {
                        targetSkill.type = type.fullMatch;
                        allJobs.push(targetSkill);
                    }
               }
            }else{
                targetSkill.type = type.notMatch;
                allJobs.push(targetSkill);
            }
        }
        if(additionSkills.length) {
            allJobs = allJobs.concat(additionSkills);
        }
        allJobs.sort( compare );
        return allJobs;
    }

    function compare( a, b ) {
        if ( a.type < b.type ){
          return -1;
        }
        if ( a.type > b.type ){
          return 1;
        }
        return 0;
      }

    return(
        <div className="job-detail">
            <div className="job-title">
                <Typography variant="h6" className={classes.title}>
                    {targetJob.nom}-({targetJob.rate}%)
                </Typography>
            </div>
            <div className="job-description">
                <Typography variant="h6" className={classes.title}>
                    Description :
                </Typography>
                <div className="detail">
                <h6>
                    {targetJob.description}
                </h6>
                </div>
            </div>
            <div className="job-skills">
                <Typography variant="h6" className={classes.title}>
                    Compétences :
                </Typography>
                <div className="skills">
                <Grid item xs={12}>
                    <div className={classes.demo}>
                        <List>
                            {
                                compareSkills().map(level => {
                                    return(
                                        level ?
                                        <ListItem>
                                            <ListItemText
                                                primary={level.competence ? level.competence.nom+"  (Niveau "+level.numero_niveau +")" : "Not found"}
                                            />
                                                {level.type == 1 ?
                                                <Icon className="fa fa-check-circle" fontSize="small" style={{ color: green[500] }} />
                                                : level.type == 2 ?
                                                <Icon className="fa fa-check-circle" fontSize="small" style={{ color: yellow[500] }} /> 
                                                : level.type == 3 ?
                                                <Icon className="fa fa-times-circle" fontSize="small" style={{ color: red[500] }} /> 
                                                : level.type == 4 ?
                                                <Icon className="fa fa-check-circle" fontSize="small" style={{ color: blue[800] }} /> 
                                                : null
                                            }
                                        </ListItem>
                                        : 
                                        null
                                             
                                    )
                                })
                            }
                        </List>
                    </div>
                    </Grid>
                </div>
            </div>
        </div>
    )
}

export default JobDetail;