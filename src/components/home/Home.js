import React , { Component } from 'react';
import axios from 'axios';
import { Container, Row, Col} from 'reactstrap';
import { json } from 'd3';
import Graph from '../graph/Graph';
import JobDetail from './JobDetail';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Button } from '@material-ui/core'
import './Home.css';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import * as docx from "docx";
import { Paragraph, AlignmentType, Header, Footer, TextRun, PageNumber, Table, TableCell, TableRow, HeadingLevel } from "docx";
import { saveAs } from 'file-saver';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            emplois: [],
            choixEmploi: null,
            selectedJob: null,
            globalScore: 0,
            matchedJobs: [],
            jobDetail: null,
            dataSet: [],
            skills: [],
        }
        this.handleChange = this.handleChange.bind(this);
        this.sortJobs = this.sortJobs.bind(this);
        this.getJobBySkills = this.getJobBySkills.bind(this);
        this.getSelectedJob = this.getSelectedJob.bind(this);
        this.getDataSet = this.getDataSet.bind(this);
        // this.generatePDF = this.generatePDF.bind(this);
        this.generateWord = this.generateWord.bind(this);
    }   
    
    UNSAFE_componentWillMount() {
        axios.get(`http://localhost:3000/emplois/getAll`)
          .then(res => {
            const jobs = res.data.data;
            this.setState({ emplois: jobs });
            console.log(res)
          })
          axios.get('http://localhost:3000/competences/getAll')
        .then(skill => {
          const allSkills = skill.data;
          this.setState({ skills: allSkills });
          
        })
      }
    
    

    sortJobs(jobList) {
        var relatedJobs = jobList.data;
        var matchedJobs = [{}];

        var selectedJob = this.state.selectedJob;
        var levelsOfSelectedJob = selectedJob.levels;
        var selectedScore = this.state.globalScore;
        var allJobs = this.state.emplois
        for(var i = 0; i < relatedJobs.length; i++) {
            var levels = relatedJobs[i].levels;
            var myJob = relatedJobs[i];
            var rate = 0;
            var sumPoint = 0;
            for(var j = 0 ; j < levels.length; j++) {
                var myLevel = levels[j];

                levelsOfSelectedJob.map(mySeletedLevel => {
                    if(myLevel.id_competence == mySeletedLevel.id_competence) {
                        if(myLevel.numero_niveau == mySeletedLevel.numero_niveau) {
                            sumPoint += myLevel.numero_niveau;
                        }else if(myLevel.numero_niveau > mySeletedLevel.numero_niveau) {
                            sumPoint += mySeletedLevel.numero_niveau;
                        }else if(myLevel.numero_niveau < mySeletedLevel.numero_niveau) {
                            sumPoint += myLevel.numero_niveau;
                        }
                    }
                })
                // Calcul du max de point
                var jobsRates = allJobs[i].levels;
                var sumRate = 0;
                jobsRates.map(levels => {
                sumRate += levels.numero_niveau;
                })
    
            }
            rate = parseInt(100 - (sumRate - sumPoint) / sumRate * 100);
            myJob.rate = rate > 100 ? 100 : rate;
            matchedJobs.push(myJob)
        }
        var dataSet = this.getDataSet(matchedJobs);       
        this.setState({
            matchedJobs,
            dataSet
        })
    }

    getDataSet(data) {
        var jobList = data;
        let R = 100;
        let c = { x: 100, y: 100 };
        let dataset = [];
        for(var i = 0; i< jobList.length; i++) {
            var job = jobList[i];
            let a = Math.random() * 2 * Math.PI;// angle
            let r = R  - job.rate;
            if(r < 5) r = 5;
            // x and y coordinates of the particle
            let x = c.x + r * Math.cos(a);
            let y = c.y + r * Math.sin(a);
            jobList[i].axis = {x,y}
        }
        
          return jobList;
    }

    getSelectedJob(selectedJob){
        this.setState({
            rerender: false,
            jobDetail: selectedJob
        })
    }
   
      
      handleChange(event) {
          var name = event.target.name;
          var value = event.target.value;
          var skillsArray = [];
          var globalScore = 0;
          
          var selectedJob = JSON.parse(value);
          var levels = selectedJob.levels;
          levels.map(level => {
            globalScore += level.numero_niveau;
            skillsArray.push(level.id_competence);
          })
          this.setState({
            selectedJob: JSON.parse(value),
            globalScore: globalScore
        })
          this.getJobBySkills(skillsArray);
      }
    getJobBySkills(skills) {
        axios.get('http://localhost:3000/emplois/getAll') // getBySkills?skills=[' +  skills + ']')
        .then(res => {
          const jobs = res.data;
          this.sortJobs(jobs);
        })
    }
    
//     generatePDF(){
//         var doc = new jsPDF('p', 'pt');
//         var jobList = this.state.emplois;

// // Page de garde
//         doc.setFont('times', 'italic')
//         doc.setFontSize(12)
//         doc.text(30, 65, 'referentiel de competences')
//         doc.addPage();      

// // Partie compétences
//         var skills = this.state.skills;

//             for(var i = 0; i < skills.length; i++) {
//                 var id_skills = skills[i].id;
//                 var listJobsLevel1 = [];
//                 var listJobsLevel2 = [];
//                 var listJobsLevel3 = [];
            
//             var skillName = skills[i].nom;
//             var skillDesc = skills[i].description;
//             var sousDomaine = skills[i].sous_domaine;
            
//             doc.rect(30, 30, 540, 90);
//             doc.setFont('helvetica', 'regular')
//             doc.setFontSize(20)
//             doc.text(40, 55, skillName, {maxWidth: 150})
//             doc.setFont('helvetica', 'italic')
//             doc.setFontSize(15)
//             doc.text(200, 55, skillDesc, {maxWidth: 340, align: "justify"})

//                 for(var j = 0 ; j < jobList.length; j++){
//                     var jobLevels = jobList[j].levels
//                     var jobName = jobList[j].nom

//                     for( var h = 0; h < jobLevels.length; h++){
//                         var idSkills = jobLevels[h].id_competence
//                         var numLevel = jobLevels[h].numero_niveau
//                         var numEmploiAttached = []
//                         if(idSkills == id_skills && numLevel == 1){
//                             listJobsLevel1.push(jobName)
//                             numEmploiAttached += 1
//                         }
//                         else if(idSkills == id_skills && numLevel == 2){
//                             listJobsLevel2.push(jobName)
//                             numEmploiAttached += 1
//                         }
//                         else if(idSkills == id_skills && numLevel == 3){
//                             listJobsLevel3.push(jobName)
//                             numEmploiAttached += 1
//                         }
//                     } 
//                 }
            
//             console.log(numEmploiAttached)

//             doc.autoTable({
//                 margin: {top: 150},
//                 head: [['Niveau 1', 'Niveau 2', 'Niveau 3']],
//                 body: [[listJobsLevel1, listJobsLevel2, listJobsLevel3]],
//               });
              
//             doc.addPage();    
//         }
           

// // Partie emploi
//         for(var i = 0; i < jobList.length; i++) {
//             var jobName = jobList[i].nom;
//             var jobDesc = jobList[i].description;
//             var levels = jobList[i].levels;
//             doc.rect(30, 30, 540, 90);
//             doc.setFont('helvetica', 'regular')
//             doc.setFontSize(25)
//             doc.text(40, 55, jobName, {maxWidth: 150})

//             doc.setFont('helvetica', 'italic')
//             doc.setFontSize(15)
//             doc.text(200, 55, jobDesc, {maxWidth: 320, align: "justify"})

//             // récupération niveau de compétence
//             for(var j = 0 ; j < levels.length; j++) {
//                 var niveau =levels[j].numero_niveau;
//                 var skills = levels[j].competence;
//                 var justifs = levels[j].emploi_niveaux;
//                 var skillName = skills.nom ? skills.nom: "NA"
//                 var skillJustif = justifs.justification ? justifs.justification: "NA"
//                 var sousDomaine = skills.sous_domaine ? skills.sous_domaine.nom : "NA"
                    
//                 doc.setFont('times', 'bold')
//                 doc.setFontSize(12)
//                 var y = 165 + 50*j
//                 // if(y>500){
//                 //     doc.addPage(); 
//                 // }
//                 doc.text(130, y, "Niveau " + niveau + ' : ' + skillName, {maxWidth: 460})
                
//                 doc.setFont('times', 'italic')
//                 doc.setFontSize(12)
//                 var x = 180 + 50*j
//                 doc.text(130, x, skillJustif, {maxWidth: 440, align: "justify"})

//                 doc.setFont('times', 'italic')
//                 doc.setFontSize(15)
//                 doc.text(30, 165 + 50*j, sousDomaine, {maxWidth: 440})
//                 doc.autoTable({
//                     margin: {top: 150},
//                     head: [['Niveau 1', 'Niveau 2', 'Niveau 3']],
//                     body: [[niveau, sousDomaine, skillJustif]],
//                   }); 
//             }   
//              doc.autoTable({
//                 margin: {top: 150},
//                 head: [['Niveau 1', 'Niveau 2', 'Niveau 3']],
//                 body: [[niveau, sousDomaine, skillJustif]],
//               });
//              doc.addPage();      
//             }
//         doc.setFont('times', 'italic')
//         doc.setFontSize(12)
//         doc.text(30, 65, 'fin')
//         doc.save('referenciel_de_competences.pdf')          
//     }  

generateWord() {
    const doc = new docx.Document();
    var skills = this.state.skills;
    var jobList = this.state.emplois;
    var ladate=new Date();

    console.log(ladate.getDate()+"/"+(ladate.getMonth()+1)+"/"+ladate.getFullYear())
    
    doc.addSection({
        children: [
            new Paragraph({
                text: 'Référentiel de compétences', 
                heading: HeadingLevel.HEADING_1,
                alignment: AlignmentType.CENTER,
            }),
        ]
    }) 
    
// Liste des compétences 
    doc.addSection({
        children: [
            new Paragraph({
                text: 'Liste des compétences', 
                heading: HeadingLevel.HEADING_1,
                alignment: AlignmentType.CENTER,
            }),
        ]
    })
    for(var i = 0; i < skills.length; i++) {
        var id_skills = skills[i].id;

        var listJobsLevel1 = [];
        var listJobsLevel2 = [];
        var listJobsLevel3 = [];

        var skillName = skills[i].nom;
        var skillDesc = skills[i].description;
        var sousDomaine = skills[i].sous_domaine;
        var nomDomaine = sousDomaine.nom;

        for(var j = 0 ; j < jobList.length; j++){
            var jobLevels = jobList[j].levels
            var jobName = jobList[j].nom

            for( var h = 0; h < jobLevels.length; h++){
                var idSkills = jobLevels[h].id_competence
                var numLevel = jobLevels[h].numero_niveau

                var numEmploiAttached = []
                if(idSkills == id_skills && numLevel == 1){
                    listJobsLevel1.push(jobName)
                    numEmploiAttached += 1
                }
                else if(idSkills == id_skills && numLevel == 2){
                    listJobsLevel2.push(jobName)
                    numEmploiAttached += 1
                }
                else if(idSkills == id_skills && numLevel == 3){
                    listJobsLevel3.push(jobName)
                    numEmploiAttached += 1
                }
            } 
        }
        doc.addSection({
            headers: {
                default: new Header({
                    children: [new Paragraph(nomDomaine)],
                }),
            },
            footers: {
                default: new Footer({
                    children: [new Paragraph("Référentiel de compétences - version du "+ ladate.getDate()+"/"+(ladate.getMonth()+1)+"/"+ladate.getFullYear())],
                }),
            },
            children: [
                new Paragraph({
                    text: skillName, 
                    heading: HeadingLevel.HEADING_2,
                    alignment: AlignmentType.CENTER,
                }),
                new Paragraph({text: skillDesc}), 
            ]
        })                                                  
    }

// Liste des emplois
    doc.addSection({
        children: [
            new Paragraph({
                text: 'Liste des emplois', 
                heading: HeadingLevel.HEADING_1,
                alignment: AlignmentType.CENTER,
            }),
        ]
    })
    for(var i = 0; i < jobList.length; i++) {
        var jobName = jobList[i].nom;
        var famille = jobList[i].direction.nom;
        var jobDesc = jobList[i].description;
        var levels = jobList[i].levels;

        for(var j = 0 ; j < levels.length; j++) {
        
            var niveau =levels[j].numero_niveau;
            var skills = levels[j].competence;
            var justifs = levels[j].emploi_niveaux;
            var skillName = skills.nom ? skills.nom: "NA"
            var skillJustif = justifs.justification ? justifs.justification: "NA"
            var sousDomaine = skills.sous_domaine ? skills.sous_domaine.nom : "NA"
            
            console.log(sousDomaine)
            console.log('Niveau '+ niveau + ' : ' + skillName)
            console.log(skillJustif)
        }           
        doc.addSection({
            headers: {
                default: new Header({
                    children: [new Paragraph(famille)],
                }),
            },
            footers: {
                default: new Footer({
                    children: [new Paragraph("Référentiel de compétences - version du "+ ladate.getDate()+"/"+(ladate.getMonth()+1)+"/"+ladate.getFullYear())],
                }),
            },
            children: [
                new Paragraph({
                    text: jobName, 
                    heading: HeadingLevel.HEADING_2,
                    alignment: AlignmentType.CENTER,
                }),
                new Paragraph({text: jobDesc}),
            ]
        }) 
    }
    docx.Packer.toBlob(doc).then(blob => {
        saveAs(blob, "example.docx");
        console.log("Document created successfully");
    });
}
    render() {  
        const {emplois, choixEmploi, jobDetail , dataSet, matchedJobs, selectedJob} =  this.state;
        
        return (
            <Container>
                <Grid container spacing={3}
                      direction="row"
                      justify="space-between"
                      alignItems="center">
                                      
                        <Grid item xs={4}>
                            <TextField
                                id="outlined-select-currency"
                                select
                                label="Choix de l'emploi"
                                name="choixEmploi"
                                size="small"
                                onChange={this.handleChange}
                                helperText=""
                                variant="outlined"
                                fullWidth
                                >
                                {emplois.map((emploi, index) =>
                                    <MenuItem key={index} value={JSON.stringify(emploi)}>
                                        {emploi.nom}
                                    </MenuItem>
                                )}
                            </TextField>
                        </Grid>
                        <Grid item xs={4}>
                        <h1>Aires de mobilité</h1>
                            {/* <h1 className="font-weight-lighter">Aires de mobilité</h1>  */}
                        </Grid>
                        {/* <Document>
        <Text>Hello World</Text>
      </Document> */}
                        <Grid item xs={4}>
                        <Button variant="outlined" color="primary" onClick={this.generateWord}>
                            Télécharger le référentiel
                            {/* <button onClick={this.generatePDF} type="primary">Télécharger le référentiel</button>    */}
                        </Button>
                        </Grid>         
                </Grid> 

                <hr/>

                <Grid container spacing={3}>
                    <Grid item xs={6}>
                            {matchedJobs && dataSet  && 
                                <Graph
                                    jobs={matchedJobs}
                                    dataSet={dataSet}
                                    selectedJob={this.getSelectedJob}
                                    
                                />
                            }
                    </Grid>
                    <Grid item xs={6}>
                        {jobDetail && selectedJob &&
                            <JobDetail 
                                selected={selectedJob}
                                targetJob={jobDetail}
                                />
                        }
                </Grid>
                 </Grid>
            </Container>
        )
    }

}


export default Home;