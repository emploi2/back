import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import * as d3 from "d3";
import { select } from 'd3-selection';

import './Graph.css';


class Graph extends Component {
    constructor(props) {
        super(props);
        this.svg = React.createRef();
        this.state = {
            jobList: [],
            dataset: [
                {},
                {rate: 10},
                {rate: 2},
                {rate: 10},
                {rate: 4},
                {rate: 10},
                {rate: 0},
                {rate: 1},
                {rate: 10},
                {rate: 7},
                {rate: 0},
            ]}
        this.addCircle = this.addCircle.bind(this);
        this.getXandY = this.getXandY.bind(this);
        this.drawPoint = this.drawPoint.bind(this);
        this.getDataSet = this.getDataSet.bind(this);
        this.getSelectedJob = this.getSelectedJob.bind(this);
        
        this.addCircle(this.props.jobs)
    }
    componentWillReceiveProps(nextProps, nextState) {
        if(nextProps.jobs) {
            this.setState({
                jobList: nextProps.jobs
            })
        }
    }

    componentWillMount() {
    }
    componentDidUpdate() {
        d3.selectAll('.dot').remove();
        //circle.exit().remove();
        this.addCircle(this.props.jobs);
    }

    getXandY(min, max) {
        var y =  Math.random() * (max - min) + min;
        var x =  Math.random() * (max - min) + min;
        return {x, y}
    }

    drawPoint() {
        for(var i = 0; i < 10 ; i++) {
            var axis = this.getXandY(40, 129);
            var data = {axis}
            this.addCircle(data);
        }
        
    }
    getSelectedJob(job) {
        this.props.selectedJob(job);
    }

    getDataSet(data) {
        var jobList = data;
        let R = 100;
        let c = { x: 100, y: 100 };
        let dataset = [];
        for(var i = 0; i< jobList.length; i++) {
            var job = jobList[i];
            let a = Math.random() * 2 * Math.PI;// angle
            let r = R  - job.rate;
            if(r < 5) r = 5;
            // x and y coordinates of the particle
            let x = c.x + r * Math.cos(a);
            let y = c.y + r * Math.sin(a);
            jobList[i].axis = {x,y}
        }
          return jobList;
    }
   

    addCircle() {
        var jobList = this.props.dataSet;
          var div = d3.select("body").append("div")	
            .attr("class", "tooltip")				
            .style("opacity", 0);
        
        var svg = d3.select('.svg');
        svg.selectAll("g")
          .data(jobList)
          .enter()
          .append("circle")
          .attr('cy', function(d) { return d.axis.y; })
          .attr('cx', function(d) { return d.axis.x; })
          .attr('r', 2)
          .attr('class', 'dot')
          .style('fill', 'rgba(232, 126, 4, 1)')
          .style('stroke', "#ddd")
          .on("mouseover", function(d) {
            d3.select(this).attr("r", 3).style("fill", "rgb(150, 54, 148)").style("cursor", "pointer");
            div.transition()		
                .duration(200)		
                .style("opacity", .9);		
            div	.html("Titre: " + d.nom + " <br/>Similitude: "+d.rate+"%")	
                .style("left", (d3.event.pageX) + "px")		
                .style("top", (d3.event.pageY - 35) + "px");
          })
          .on("mouseout", function(d) {
            d3.select(this).attr("r", 2).style("fill", "rgba(232, 126, 4, 1)");
            div.transition()		
                .duration(500)		
                .style("opacity", 0);	
          })
          .on("click", d => {
            this.getSelectedJob(d);
            div.transition()		
                .duration(500)		
                .style("opacity", 0);	
            
          });      
    }
    
    render() {
        return (
          

       <div>
           <Row>
               <div className="chart">
                    <div className="item3"><span>0- 35%</span> </div>
                    <div className="item2"><span>35- 70%</span></div>
                    <div className="item1"> <span>70- 100%</span></div>
               </div>
           </Row>
            <svg id="svg" class="svg" ref={this.svg} viewBox="0  0 200 200" xmlns="http://www.w3.org/2000/svg" >
                <circle id="graph" cx="100" cy="100" r="100"  />

                <circle id="accuracy_0_35" cx="100" cy="100" r="100" title="50-80%" ></circle>
                <circle id="accuracy_35_70" cx="100" cy="100" r="65" title="50-80%" onclick="alert('You have clicked the circle.')"></circle>
                <circle id="accuracy_70_100" cx="100" cy="100" r="30" onclick="alert('You have clicked the circle.')" ></circle>

                <circle id="cible" cx="100" cy="100" r="3" title="80-100%" onclick="alert('You have clicked the circle.')"></circle>
                <g></g>
            </svg>
       </div>
        )
    }
}

export default Graph;