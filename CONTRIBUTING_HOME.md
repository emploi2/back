import React , { Component } from 'react';
import axios from 'axios';
import { Container, Row, Col} from 'reactstrap';
import Graph from '../graph/Graph';

class Home extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            emplois: [],
            choixEmploi: null,
            selectedJob: [],
            globalScore: 0,
            matchedJobs: [],
            skillsSelected: [],
        }
        this.handleChange = this.handleChange.bind(this);
        this.sortJobs = this.sortJobs.bind(this);
        this.getJobBySkills = this.getJobBySkills.bind(this);
        this.getSelectedJob = this.getSelectedJob.bind(this);
    }   
    
    UNSAFE_componentWillMount() {
        axios.get(`http://localhost:3000/emplois/getAll`)
          .then(res => {
            const jobs = res.data;
            this.setState({ emplois: jobs });
          })
      }

    sortJobs(jobList) {
        var relatedJobs = jobList.data;
        var matchedJobs = [{}];
        var selectedJob = this.state.selectedJob;
        var levelsOfSelectedJob = JSON.parse(selectedJob).levels;
        var allJobs = this.state.emplois   
 
    // Calcul de similitude
        for(var i = 0; i < relatedJobs.length; i++) {
            var levels = relatedJobs[i].levels;
            var myJob = relatedJobs[i];
            var rate = 0;

            for(var j = 0 ; j < levels.length; j++) {
                    var myLevel = levels[j];
                    levelsOfSelectedJob.map(mySeletedLevel => {
                        if(myLevel.id_competence == mySeletedLevel.id_competence && myLevel.numero_niveau == 1) {
                            rate += 1;
                        }
                        else if(myLevel.id_competence == mySeletedLevel.id_competence && myLevel.numero_niveau == 2){
                            if(mySeletedLevel.numero_niveau == 1) {
                                rate += 1;
                            }else if(mySeletedLevel.numero_niveau == 2) {
                                rate += 2;
                            }
                        }
                    })}
     // Calcul du max de point
        var jobsRates = allJobs[i].levels;
        var sumRate = 0;
        jobsRates.map(levels => {
        sumRate += levels.numero_niveau;
        })

            myJob.rate = parseInt(100 - ((sumRate - rate) / sumRate * 100));
            matchedJobs.push(myJob)    
        this.setState({
            matchedJobs
        })
    }
}

    getSelectedJob(selectedJob){
        var idCLickJob = selectedJob
        var listJobs = this.state.emplois
        var listSkillsFirstJob = this.state.skillsSelected
        var toLearn = [];
        var learned = [];
        var toWork = [];

        for (var i = 0; i < listJobs.length; i++){
            var myJob = listJobs[i];
            if(myJob.id_emploi == idCLickJob){
               var clickLevels = myJob.levels;
               var idFirstList = [];
               listSkillsFirstJob.forEach(level => {
                idFirstList.push(level.id_competence)
            });
           
               for (var j = 0; j < clickLevels.length ; j++){
                   var myClickLevels = clickLevels[j];

                   if(idFirstList.indexOf(myClickLevels.id_competence) === -1){
                      toLearn.push(myClickLevels)
                   }

                   listSkillsFirstJob.map(myFirstSkills => {
                   if(myFirstSkills.id_competence == myClickLevels.id_competence){
                     if(myFirstSkills.numero_niveau == myClickLevels.numero_niveau){
                        learned.push(myClickLevels)
                     }
                     if (myFirstSkills.numero_niveau > myClickLevels.numero_niveau){
                        learned.push(myClickLevels)
                     }
                     if (myFirstSkills.numero_niveau < myClickLevels.numero_niveau){
                        toWork.push(myClickLevels)
                    }
                    }                  
                })
                  }
          }
        }
        console.log(learned)
        console.log(toWork)
        console.log(toLearn)
    }
      
    handleChange(event) {
        //   var name = event.target.name;
          var value = event.target.value;
          var skillsArray = [];
          var globalScore = 0;
          
          var selectedJob = JSON.parse(value);
          var levels = selectedJob.levels;
          levels.map(level => {
            globalScore += level.numero_niveau;
            skillsArray.push(level.id_competence);
          })
          this.setState({
            selectedJob: value,
            globalScore: globalScore
        })
          this.getJobBySkills(skillsArray);
          
          this.state.skillsSelected = [];
          levels.map(level => {
            this.state.skillsSelected.push(level);
          })
      }

    getJobBySkills(skills) {
        axios.post('http://localhost:3000/emplois/getBySkills?skills=[' +  skills + ']')
        .then(res => {
          const jobs = res.data;
          this.sortJobs(jobs);
        })
    }

    render() {  
        const {emplois, skillsSelected} =  this.state;
        
    return (
        <Container>
            <Row>
                <Col className="text-center">
                    <h1 className="font-weight-lighter">Aires de mobilité</h1>
                </Col>
            </Row>
            <Row>
                <Col>                  
                <select
                    name="choixEmploi"
                    onChange = {this.handleChange}
                    >
                    <option hidden>choix</option>
                        {emplois.map((emploi, index) =>
                        <option 
                            key={index}
                            value={JSON.stringify(emploi)}>
                                {emploi.nom}
                    </option>
                        )}
                </select>
                </Col>       
            </Row>
                <hr/>
            <Row>
                <Col xs={6}>
                    <div className="job-preview">
                        {skillsSelected.map((skill, index) =>
                        <p align="justify"
                            key={index}
                            value={skill}>
                                <b>Niveau : {skill.numero_niveau}</b>
                                <br/>{skill.nom}
                        </p>
                        )}
                    </div>
                </Col>
                <Col xs={6}>
                    <Graph
                        jobs={this.state.matchedJobs}
                        selectedJob={this.getSelectedJob}
                    />
                </Col>
            </Row>
        </Container>
        )
    }
}

export default Home;